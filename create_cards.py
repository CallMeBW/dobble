from typing import FrozenSet, Generator, Optional, Set
from itertools import chain


CARD_LEN = 8
NUM_SYMBOLS = 57
#CARD_LEN = 3
#NUM_SYMBOLS = 6


class Card(FrozenSet[int]):
    def __init__(self, *_):
        super().__init__()
        assert len(self) == CARD_LEN
        for i in self:
            assert 0 <= i <= NUM_SYMBOLS

    def __repr__(self) -> str:
        mask = ["1" if i in self else "0" for i in range(NUM_SYMBOLS)]
        return "".join(mask)[::-1] + f" ({list(self)}) "


class Mask:
    bitmask: int

    def __init__(self, bitmask: int):
        self.bitmask = bitmask

    def __repr__(self) -> str:
        bit_string = "{0:b}".format(self.bitmask)
        leading_zeros = "0" * (NUM_SYMBOLS - len(bit_string))
        return f"{leading_zeros}{bit_string}"

    def mask(self, bit_index: int):
      """ bit_index is the rightwards-counted index to mask, e.g. 2 to mask 10 """
      self.bitmask |= 2**bit_index

    def unmask(self, bit_index: int):
      """ bit_index is the rightwards-counted index to mask, e.g. 2 to unmask 10 """
      self.bitmask = self.bitmask & ~(2**bit_index)

    def is_valid_card(self) -> bool:
        return bin(self.bitmask).count("1") == CARD_LEN

    def to_card(self) -> Card:
      assert self.is_valid_card()
      rev_bitstring_enum = enumerate(reversed(repr(self)))
      return Card(i for i, bit_character in rev_bitstring_enum if bit_character == "1")



def get_next_bit_index(mask: Mask) -> int:
    """ returns the first rightward bit index bigger than the most-sign. 1 bit-index """
    if mask.bitmask == 0:
        return 0
    bit_string = repr(mask) # 00100100
    #assert "1" in bit_string
    next_ind = len(bit_string) - bit_string.find("1")
    return max(0, next_ind)

def get_most_sig_bit_index(mask: Mask) -> int:
    """ returns most significant index with a 1 set, or -1 in case of bitmask being 0 """
    return get_next_bit_index(mask) - 1

def find_intersecting_cards(existing_cards: Set[Card], bit_index: int) -> Set[Card]:
    """ Returns a subset of cards that contain the bit_index """
    return set(card for card in existing_cards if bit_index in card)


def find_next_flippable_bit_index(mask: Mask, existing_cards: Set[Card], intersecting_cards: Set[Card], index_start: int = None) -> Optional[tuple[int,Set[Card]]]:
    """ """
    max_bit_index = len(repr(mask)) - 1
    if index_start is None:
        next_ind = get_next_bit_index(mask)
    else:
        next_ind = index_start
    while True:
        if next_ind > max_bit_index:
            return None
        newly_conflicting_cards = find_intersecting_cards(existing_cards, next_ind)
        if newly_conflicting_cards & intersecting_cards:
            next_ind += 1
        else:
            return next_ind, newly_conflicting_cards

# This is an old version of the algorithm, in which it was possible
# that a card doesn't match any symbol on another card
# def card_generator() -> Generator[Card, None, None]:
#     existing_cards: Set[Card] = set()
#     while True:
#         next_card_mask = Mask(1)
#         intersected_cards: Set[Card] = find_intersecting_cards(existing_cards, 0)
#         index_start = 1
#         while not next_card_mask.is_valid_card():
#             next_bit_result = find_next_flippable_bit_index(next_card_mask, existing_cards, intersected_cards, index_start)
#             most_sig_bit = get_most_sig_bit_index(next_card_mask)
#             if next_bit_result is None:
#                 next_card_mask.unmask(most_sig_bit)
#                 intersected_cards = intersected_cards - find_intersecting_cards(intersected_cards, most_sig_bit)
#                 index_start = most_sig_bit + 1
#                 if index_start >= NUM_SYMBOLS:# todo: catch this case already when num of remaining bits is less then remaining required num of 1 bits in curr mask
#                     most_sig_bit = get_most_sig_bit_index(next_card_mask)#todo: check if there is anything to unmask at all, otherwise: done finding all
#                     # todo: remove conflicting card
#                     next_card_mask.unmask(most_sig_bit)
#                     intersected_cards = intersected_cards - find_intersecting_cards(intersected_cards, most_sig_bit)
#                     if most_sig_bit >= NUM_SYMBOLS:
#                         return
#                     #next_card_mask.mask(most_sig_bit + 1)
#                     index_start = most_sig_bit + 1
#             else:
#                 next_bit, newly_intersect_cards = next_bit_result
#                 next_card_mask.mask(next_bit)
#                 intersected_cards.update(newly_intersect_cards)
#                 index_start = next_bit + 1
#         card = next_card_mask.to_card()
#         yield (card := next_card_mask.to_card())
#         existing_cards.add(card)


def card_constraint_gen() -> Generator[Card, None, None]:
    # init global vars
    existing_cards: list[Card] = list()
    global_start = 0

    # init local vars
    forbidden_set: Set[int] = set()
    next_mask = Mask(global_start)
    local_start = global_start
    to_be_matched: Set[Card] = set()
    while True:
        left_to_place = CARD_LEN - repr(next_mask).count("1")#TODO: remove from forbidden set numbers smaller than local start? #todo2: replace bin() by repr()
        if left_to_place == 0: # todo: add check if any of the cards can no longer be matched
            ### card is complete ###
            assert next_mask.is_valid_card()
            #assert to_be_matched == set(), "\n"+"\n".join(map(str, existing_cards))+"\n\n "+str(next_mask)+"\n"+str(to_be_matched)
            if to_be_matched == set():
                existing_cards.append((card := next_mask.to_card()))
                yield card
            else:
                # TODO: continue with next
                return
            # reset local variables after card has been found
            forbidden_set = set()
            next_mask = Mask(0)
            local_start = global_start
            to_be_matched: Set[Card] = set(existing_cards)
            continue
        ### set next bit of next_mask ###
        first_settable_bit = max(local_start, get_next_bit_index(next_mask))
        forbidden_set -= set(i for i in forbidden_set if i < first_settable_bit) # todo: what if first_settable_bit in forbidden_set?
        #todo: increase first_settable bit until is not in forbidden set
        num_bits_left = NUM_SYMBOLS - first_settable_bit - len(forbidden_set)
        if num_bits_left < left_to_place:
            if next_mask.bitmask == 0: # global end, cannot place any symbol anymore
                break
            if repr(next_mask).count("1") == 1: # if it was the only 1 set: global set_back
                global_start += 1
                forbidden_set = set()
                next_mask = Mask(0)
                local_start = global_start
                to_be_matched = set(existing_cards)
                continue
            unmasked_bit = get_most_sig_bit_index(next_mask)
            next_mask.unmask(unmasked_bit)
            local_start = unmasked_bit + 1
            for no_longer_intersecting_card in find_intersecting_cards(set(existing_cards), unmasked_bit):
                forbidden_set -= no_longer_intersecting_card
                to_be_matched.add(no_longer_intersecting_card)
            continue
        next_bit = next(i for i in range(first_settable_bit, NUM_SYMBOLS) if i not in forbidden_set)# it may be that we need to keep track of that
        next_mask.mask(next_bit)  # todo: check not forbidden
        newly_intersecting_cards = find_intersecting_cards(set(existing_cards), next_bit)
        to_be_matched -= newly_intersecting_cards
        forbidden_set |= set(chain(*newly_intersecting_cards))
        for c in newly_intersecting_cards:
            for num in c:
                forbidden_set.add(num)

def main():
    cards = list(card_constraint_gen())
    print("\n".join(map(str, cards)))
    for c1 in cards:
        for c2 in cards:
            if c1 == c2:
                continue
            intersection = c1 & c2
            assert len(intersection) == 1, f"\n{c1}\n{c2}"
    print("total: ", len(cards))


if __name__ == "__main__":
    main()

